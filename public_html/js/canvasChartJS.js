var chartJS = {
    init: function () {
        console.log("Initializing chartJS...")
        this.bar.init();
    },
    bar: {
        init: function () {
            var barTables = document.querySelectorAll("table[data-type=bar]");
            for (var i = 0; i < barTables.length; i++) {
                this.draw(i, barTables[i]);
            }
        },
        draw: function (n, table) {
            var offset = 10;
            var width = 150;
            var maxHeight = 300;
            var maxValue = 0;
            var labelSize = 25;
            for (var i = 0; i < table.rows.length; i++) {
                var currentValue = parseInt(table.rows[i].cells[1].innerHTML);
                if (currentValue > maxValue) {
                    maxValue = currentValue;
                }
            }
            console.log("BarChart " + n + " highest value is " + maxValue);
            var canvas = document.createElement("CANVAS");
            canvas.height = maxHeight + labelSize;
            canvas.width = ((width*table.rows.length) + (offset * table.rows.length));
            table.parentNode.insertBefore(canvas, table.nextSibling);
            var context = canvas.getContext("2d");
            for (var i = 0; i < table.rows.length; i++) {
                // Init vars to work with
                var x = (offset * i) + (width * i);
                var label = table.rows[i].cells[0].innerHTML;
                var currentValue = parseInt(table.rows[i].cells[1].innerHTML);
                var height = maxHeight * (currentValue / maxValue);
                var y = maxHeight - height;
                var barColor = color.str(label);
                // Draw bar
                context.fillStyle = barColor;
                context.fillRect(x, y, width, height);
                // Draw value
                context.fillStyle = color.contrast(barColor);
                context.font = labelSize + "px Georgia";
                context.textAlign = "center";
                context.fillText(currentValue, x+(width/2), y+labelSize)
                // Draw label
                context.fillStyle = "#000000";
                context.font = labelSize + "px Georgia";
                context.textAlign = "center";
                context.fillText(label, x+(width/2), y + height + labelSize);
            }
        }
    }
};

var color = {
    random: function () {
        return '#' + Math.floor(Math.random() * 16777215).toString(16);
    },
    str: function (str) {
        return '#' + int.toRGB(string.hash(str));
    },
    contrast: function(hexcolor) {
	var r = parseInt(hexcolor.substr(0,2),16);
	var g = parseInt(hexcolor.substr(2,2),16);
	var b = parseInt(hexcolor.substr(4,2),16);
	var yiq = ((r*299)+(g*587)+(b*114))/1000;
	return (yiq >= 128) ? '#000000' : '#ffffff';
    }
};

var string = {
    hash: function (str) { // java String#hashCode
        var hash = 0;
        for (var i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        return hash;
    }
};

var int = {
    toRGB: function (i) {
        var c = (i & 0x00FFFFFF)
                .toString(16)
                .toUpperCase();

        return "00000".substring(0, 6 - c.length) + c;
    }
};

chartJS.init();